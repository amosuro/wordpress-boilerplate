                <div class="push"></div>
                </div> <!-- end .wrapper -->
                <footer class="main-footer">
                    <div class="row">
            			<div class="large-12 columns">
            				<p>&copy; <?php echo date("Y"); echo " "; bloginfo('name'); ?></p>
            			</div>
                    </div>
        		</footer>
                <a class="exit-off-canvas"></a>
            </div><!-- end inner-wrap -->
        </div><!-- end outer-wrap -->
	<?php wp_footer(); ?>
</body>

</html>
