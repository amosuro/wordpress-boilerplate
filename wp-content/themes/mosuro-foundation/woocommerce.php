<?php get_header(); ?>

<section id="shop-main" class="row">
    <?php woocommerce_content(); ?>
</section>

<?php get_footer(); ?>
