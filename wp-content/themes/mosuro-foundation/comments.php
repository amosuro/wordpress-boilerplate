<?php

	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		This post is password protected. Enter the password to view comments.
	<?php
		return;
	}
?>



<?php // ---------------- this function will be used to style the listed comments
function advanced_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
 
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
   <div class="comment-author vcard">
       <div class="comment-meta"><?php printf(__('%s'), comment_author()) ?></div>
       <small><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?><?php edit_comment_link(__('(Edit)'),'  ','') ?></small>
     </div>
     <div class="clear"></div>
 
     <?php if ($comment->comment_approved == '0') : ?>
       <em><?php _e('Your comment is awaiting moderation.') ?></em>
       <br />
     <?php endif; ?>
 
     <div class="comment-text">	
         <?php comment_text() ?>
     </div>
 
   <div class="reply">
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
   </div>
   <div class="clear"></div>
<?php } // ---------------- End function to be used to style listed comments ?>





<?php if ( have_comments() ) : ?>
	<section id="commentContainer">
		<section id="listComments">
			<h2 id="comments"><?php comments_number('No Responses', 'One Response', '% Responses' );?></h2>
		
			<div class="navigation">
				<div class="next-posts"><?php previous_comments_link() ?></div>
				<div class="prev-posts"><?php next_comments_link() ?></div>
			</div>
		
			<ol class="commentlist">
				<?php wp_list_comments('type=comment&callback=advanced_comment'); ?>
			</ol>
		
			<div class="navigation">
				<div class="next-posts"><?php previous_comments_link() ?></div>
				<div class="prev-posts"><?php next_comments_link() ?></div>
			</div>
		</section>
		<!-- End listComments -->
		<?php if ( comments_open() ) : ?>
		<section id="respond">
		
			<h2><?php comment_form_title( 'Leave a Reply', 'Leave a Reply to %s' ); ?></h2>
		
			<div class="cancel-comment-reply">
				<?php cancel_comment_reply_link(); ?>
			</div>
		
			<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
				<p>You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>">logged in</a> to post a comment.</p>
			<?php else : ?>
		
			<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		
				<?php if ( is_user_logged_in() ) : ?>
		
					<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>
		
				<?php else : ?>
		
					<div>
						<input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
						<label for="author">Name <?php if ($req) echo "(required)"; ?></label>
					</div>
		
					<div>
						<input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
						<label for="email">Mail (will not be published) <?php if ($req) echo "(required)"; ?></label>
					</div>
		
					<div>
						<input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
						<label for="url">Website</label>
					</div>
		
				<?php endif; ?>
		
				<!--<p>You can use these tags: <code><?php echo allowed_tags(); ?></code></p>-->
		
				<div>
					<textarea name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea>
				</div>
		
				<div class="submitComment">
					<input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
					<?php comment_id_fields(); ?>
				</div>
				
				<?php do_action('comment_form', $post->ID); ?>
		
			</form>
		
			<?php endif; // If registration required and not logged in ?>
			
		</section><!-- End respond -->

	</section><!-- End commentsContainer -->
	
	<?php else : // this is displayed if there are no comments so far ?>
			
	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->
	 <?php else : // comments are closed ?>
		<p>Comments are closed.</p>
	<?php endif; ?>
	
	<?php endif; ?>

<?php endif; ?>
