<?php get_header(); ?>

<section class="row">
    <div class="large-8 medium-8 small-12 columns">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

                <h1 class="entry-title"><?php the_title(); ?></h1>
                <?php include (TEMPLATEPATH . '/_/inc/post-info.php' ); ?>
                <div class="entry entry-content">

                    <?php the_content(); ?>

                    <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

                    <?php the_tags( 'Tags: ', ', ', ''); ?>

                    <?php include (TEMPLATEPATH . '/_/inc/post-info.php' ); ?>

                </div>

            </article>

            <?php comments_template(); ?>

        <?php endwhile; endif; ?>
    </div>

    <div class="large-4 medium-4 small-12 columns">
        <?php get_search_form(); ?>
        <h2>Categories</h2>
        <ul>
            <?php wp_list_categories('exclude=4,7&title_li='); ?>
        </ul>
        <div id="jstwitter"></div>
    </div>
</section>

<?php get_footer(); ?>