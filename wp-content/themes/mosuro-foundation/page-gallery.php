<?php

/* Template Name: DEMO Image Gallery */

get_header(); ?>

<script type="text/javascript">
    $(function() {
        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,
            arrows    : false,
            nextClick : true,

            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });
    });
</script>

<section class="row">
    <div class="large-12 columns">
        <ul class="small-block-grid-2 large-block-grid-4">
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="http://placehold.it/500x500.jpg"><img src="http://placehold.it/500x500" alt="" class="img-responsive" /></a>
            </li>
        </ul>
    </div>
</section>


<?php get_footer(); ?>
