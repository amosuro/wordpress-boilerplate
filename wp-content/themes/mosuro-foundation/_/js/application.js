// Extend JS String with repeat method
String.prototype.repeat = function(num) {
    return new Array(num + 1).join(this);
};

// WP & Foundation compatibility
function foundationWP() {
    // Initialize Foundation
    $(document).foundation();

    // Wordpress sub-nav compatibility w/Foundation
    $('.menu-item-has-children').addClass('has-dropdown');
    $('.sub-menu').addClass('dropdown');
    // Wordpress posts navigation & Foundation pagination styling
    $('.post-navigation ul').addClass('pagination');
    $('.post-navigation .next, .post-navigation .prev').parent().addClass('unavailable arrow');
}
foundationWP();

// WP & Bootstrap compatibility
function bootstrapWP() {

}
bootstrapWP();

(function($) {

    // Misc functions
    $(function() {
        // Disable link clicks to prevent page scrolling
        $('a[href="#fakelink"]').on('click', function (e) {
            e.preventDefault();
        });
    });

    // Form Validation
    function formValidation() {
        $('#form-response').hide();
        $('#form-contact').ajaxForm({
            target: '#form-response',
            success: function() {
            $('#form-response').fadeIn('slow').delay(3000).fadeOut('slow');
            }
        });
    }


})(jQuery);