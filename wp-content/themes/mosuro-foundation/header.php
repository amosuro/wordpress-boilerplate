<!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="no-js ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head data-template-set="creative-theme" profile="http://gmpg.org/xfn/11">
	<!-- include the meta tags -->
	<?php include (TEMPLATEPATH . '/_/inc/meta-tags.php' ); ?>
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/_/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/_/img/apple-touch-icon.png">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<script>
        var templateDir = "<?php bloginfo('template_directory') ?>";
    </script>
	<!-- Asynchronous google analytics; this is the official snippet.
		 Replace UA-XXXXXX-XX with your site's ID and uncomment to enable
	<script>
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', gaID]);
	  _gaq.push(['_trackPageview']);
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
    -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="off-canvas-wrap" data-offcanvas>
        <div class="inner-wrap">
            <a class="canvas-toggle left-off-canvas-toggle hide-for-medium-up"><span></span></a>
            <!-- Off Canvas Menu -->
            <aside class="canvas-menu left-off-canvas-menu">
                <?php $mobile_menu_args = array (
                    'theme_location' => 'mobile-menu',
                    'container' => false,
                    'menu_class' => 'no-bullet'
                    );
                ?>
                <?php wp_nav_menu( $mobile_menu_args ); ?>
            </aside>
            <div class="wrapper">
                <div class="contain-to-grid show-for-medium-up">
                    <nav class="top-bar" data-topbar>
                        <ul class="title-area">
                          <!-- Title Area -->
                          <li class="name">
                            <h1>
                              <a href="<?php echo home_url(); ?>">
                                Brand
                              </a>
                            </h1>
                          </li>
                          <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                        </ul>

                        <section class="top-bar-section">
                          <?php $primary_menu_args = array (
                                'theme_location' => 'primary-menu',
                                'container' => false,
                                'menu_class' => 'right'
                                );
                            ?>
                            <?php wp_nav_menu( $primary_menu_args ); ?>
                        </section>
                      </nav>
                  </div>
