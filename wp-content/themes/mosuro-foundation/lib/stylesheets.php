<?php

/* Stylesheets
================================================== */

function custom_stylesheets() {
	global $wp_styles;
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), 'all' );
		wp_enqueue_style( 'app', get_template_directory_uri() . '/_/css/app.css', array(), 'all' );

	wp_enqueue_style( 'ie', get_template_directory_uri() . '/_/css/ie.css', array(), 'all' );
	$GLOBALS['wp_styles']->add_data( 'ie', 'conditional', 'lt IE 9' );

	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/_/js/fancybox/jquery.fancybox.css', array(), '20120208', 'all' );
	wp_enqueue_style( 'fancybox-buttons-css', get_template_directory_uri() . '/_/js/fancybox/helpers/jquery.fancybox-buttons.css', array(), '20120208', 'all' );
	wp_enqueue_style( 'fancybox-thumbs-css', get_template_directory_uri() . '/_/js/fancybox/helpers/jquery.fancybox-thumbs.css', array(), '20120208', 'all' );
}
add_action( 'wp_enqueue_scripts', 'custom_stylesheets' );
