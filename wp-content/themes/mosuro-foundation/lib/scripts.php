<?php

/* Scripts
================================================== */

function custom_scripts() {
    global $wp_scripts;
    wp_deregister_script('jquery');
    if(!is_admin()) {
         wp_register_script('jquery', 'http://code.jquery.com/jquery-2.0.3.min.js');
         wp_enqueue_script('jquery');
    }
    // Jquery UI
    wp_enqueue_script('jquery-ui', get_template_directory_uri() . "/_/js/jquery-ui-1.10.3.custom.min.js", array('jquery'), '', false);
    // Foundation
    wp_enqueue_script('foundation', get_template_directory_uri().'/_/js/foundation/foundation.js', '', '', true);
    wp_enqueue_script('foundation-top-bar', get_template_directory_uri().'/_/js/foundation/foundation.topbar.js', '', '', true);
    wp_enqueue_script('foundation-offcanvas', get_template_directory_uri().'/_/js/foundation/foundation.offcanvas.js', array( 'jquery' ), '', true);
    // Modernizr
    wp_enqueue_script('modernizr', get_template_directory_uri().'/_/js/custom.modernizr.js', array( 'jquery' ), '', false);
    // Fancybox main scripts
    wp_enqueue_script('jquery-fancybox', get_template_directory_uri() . '/_/js/fancybox/js/jquery.fancybox.pack.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-fancybox-mousewheel', get_template_directory_uri() . '/_/js/fancybox/js/jquery.mousewheel-3.0.6.pack.js', array('jquery'), '', true);
    // Fancybox helper scripts
    wp_enqueue_script('jquery-fancybox-buttons', get_template_directory_uri() . '/_/js/fancybox/helpers/jquery.fancybox-buttons.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-fancybox-media', get_template_directory_uri() . '/_/js/fancybox/helpers/jquery.fancybox-media.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-fancybox-thumbs', get_template_directory_uri() . '/_/js/fancybox/helpers/jquery.fancybox-thumbs.js', array('jquery'), '', true);

    // Form validation
    // wp_enqueue_script('form', get_template_directory_uri().'/_/js/form.js', array( 'jquery' ), '', true);

    // Application.js (all custom scripts)
    wp_enqueue_script('application', get_template_directory_uri().'/_/js/application.js', array( 'jquery' ), '', true);

    // IE Fixes
        // HTML5 Shiv for
        wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/_/js/html5shiv.js', array(), '', false);
        $GLOBALS['wp_scripts']->add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        // NWMatcher
        wp_enqueue_script( 'nwmwatcher', '//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js', array(), '', false);
        $GLOBALS['wp_scripts']->add_data( 'nwmwatcher', 'conditional', 'lt IE 9' );
        // Selectivizr
        wp_enqueue_script( 'selectivizr', '//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js', array(), '', false);
        $GLOBALS['wp_scripts']->add_data( 'selectivizr', 'conditional', 'lt IE 9' );
        // Respond.js
        wp_enqueue_script( 'respond', '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js', array(), '', false);
        $GLOBALS['wp_scripts']->add_data( 'respond', 'conditional', 'lt IE 9' );
        // REM polyfill
        wp_enqueue_script( 'rem-polyfill', get_template_directory_uri() . '/_/js/rem.min.js', array(), '', false);
        $GLOBALS['wp_scripts']->add_data( 'rem-polyfill', 'conditional', 'lt IE 9' );


}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
