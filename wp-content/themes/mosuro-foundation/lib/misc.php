<?php

/* Misc
================================================== */


// Add RSS links to <head> section
add_theme_support( 'automatic-feed-links' );

// Thumbnail support
add_theme_support( 'post-thumbnails' );

// Post format support
add_theme_support( 'structured-post-formats', array(
    'link', 'video'
));
add_theme_support( 'post-formats', array(
    'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status'
));

// Register custom nav menus
register_nav_menus(
	array(
	  'primary-menu' => __( 'Primary Navigation', 'pw_lang' ),
      'mobile-menu' => __( 'Mobile Navigation', 'pw_lang' )
	)
);

// Allow custom fields
function get_custom_field_value($szKey, $bPrint = false) {
    global $post;
    $szValue = get_post_meta($post->ID, $szKey, true);
    if ( $bPrint == false ) return $szValue; else echo $szValue;
}

// Remove admin bar
add_filter( 'show_admin_bar', '__return_false' );

// Customize the excerpt text
function new_excerpt_more($more) {
    global $post;
	return '<a href="'. get_permalink($post->ID) . '"> (...read more)</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Control except length
function word_count($string, $limit) {
    $words = explode(' ', $string);
    return implode(' ', array_slice($words, 0, $limit));
    /*
        Use this code: <?php echo word_count(get_the_excerpt(), '30'); ?>
    */
}

// Tell admin area editor to look at this stylesheet
add_editor_style(get_bloginfo('template_directory').'_/css/editor-style.css');

// Woocommerce support
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);
function my_theme_wrapper_start() {
  echo '<section id="shop-main"><div class="container">';
}

function my_theme_wrapper_end() {
  echo '</div></section>';
}
add_theme_support( 'woocommerce' );

// Add the form class to Contact Form 7 plugin to maintain Foundation styling
function your_custom_form_class_attr( $class ) {
    $class .= ' form';
    return $class;
}