<?php 
// Product Type

	function register_tax_product_type() {
	    register_taxonomy(
	        'product_type',
	        'product',
	        array(
	            'labels' => array(
	                'name' => 'Product Types',
	                'add_new_item' => 'Add New Product Types',
	                'new_item_name' => "New Product Type"
	            ),
	            'show_ui' => true,
	            'show_tagcloud' => false,
	            'hierarchical' => true
	        )
	    );
	}

	add_action( 'init', 'register_tax_product_type', 0 );

	/* To display taxonomies in a loop:
		
		<?php

		$taxonomy = 'product_type';
		$queried_term = get_query_var($taxonomy);
		$terms = get_terms($taxonomy, 'slug='.$queried_term);
		if ($terms) {
		  echo '<ul>';
		  foreach($terms as $term) {
		  	echo '<li><a href="'.get_term_link($term->slug, $taxonomy).'">'.$term->name.'</a></li>';
		  }
		  echo '</ul>';
		}

		?>

    */
?>