<?php
	function register_cpt_product() {
		register_post_type( 'product',
	        array(
	            'labels' => array(
	                'name' => 'Products'
	            ),
	            'public' => true,
	            'supports' => array( 'title', 'editor', 'thumbnail' ),
	            'taxonomies' => array( '' ),
	            'has_archive' => true
	        )
	    );
	}
	add_action( 'init', 'register_cpt_product' );

	/* To display custom post in a loop:

	<?php $wp_query = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1, 'orderby' => 'date', 'order'=> 'ASC', 'paged' => $paged )); ?>
	<?php include (TEMPLATEPATH . '/_/inc/post-nav.php' ); ?> // Pagination
    <?php while ($wp_query->have_posts() ) : $wp_query->the_post(); ?>
	<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );?>
    	   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo $src[0]; ?>" alt="<?php the_title(); ?>" /></a>
	<?php endwhile; ?>
    <?php include (TEMPLATEPATH . '/_/inc/post-nav.php' ); ?> // Pagination

	*/
?>