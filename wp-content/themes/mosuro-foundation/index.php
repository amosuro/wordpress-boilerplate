<?php get_header(); ?>

<section class="row">
	<div class="large-8 medium-8 small-12 columns">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				<?php include (TEMPLATEPATH . '/_/inc/post-info.php' ); ?>
				<div class="entry">
					<?php the_excerpt(); ?>
				</div>
				<footer class="postmetadata">
					<p><?php the_tags('Tags: ', ', ', '<br />'); ?></p>
					<p><strong>Posted in: </strong> <?php the_category(', ') ?> | <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
				</footer>
			</article>
		<?php endwhile; ?>
		<?php include (TEMPLATEPATH . '/_/inc/post-nav.php' ); ?>
		<?php else : ?>
			<h2>Not Found</h2>
		<?php endif; ?>
	</div>

	<div class="large-4 medium-4 small-12 columns">
		<?php get_search_form(); ?>
		<h2>Categories</h2>
		<ul>
			<?php wp_list_categories('exclude=4,7&title_li='); ?>
		</ul>
		<div id="jstwitter"></div>
	</div>
</section>


<?php get_footer(); ?>