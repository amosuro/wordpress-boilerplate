<?php

/* Template Name: Contact */

/* Spam prevention */
$randomQuestions = array(
                       array( "question" => "What colour is the sky?", "answer" => "blue" ),
                       array( "question" => "Whilst driving, what colour do you stop for?", "answer" => "red" ),
                       array( "question" => "What colour is the grass?", "answer" => "green" )
                  );

$singleQuestion = $randomQuestions[array_rand( $randomQuestions )];


session_start();
$_SESSION['antiSpamAnswer'] = $singleQuestion['answer'];
/* Spam prevention */

get_header();
?>

<section class="row">
    <div class="large-12 columns">
        <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

            <form action="<?php bloginfo('template_directory'); ?>/_/inc/submit-contact.php" method="post" name="form-contact" id="form-contact" class="form">

                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="Name">Name</label>
                            <input type="text" name="Name" id="Name" class="form-control" placeholder="Enter Name" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="Company">Company</label>
                            <input type="text" name="Company" id="Company" class="form-control" placeholder="Enter Company" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="EmailAddress">Email Address</label>
                            <input type="text" name="EmailAddress" id="EmailAddress" class="form-control" placeholder="Enter Email address" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="PhoneNumber">Phone Number</label>
                            <input type="text" name="PhoneNumber" id="PhoneNumber" class="form-control" placeholder="Enter Phone number" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="PhoneNumber">Phone Number</label>
                            <input type="text" name="PhoneNumber" id="PhoneNumber" class="form-control" placeholder="Enter Phone number" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="Message">Message</label>
                            <textarea name="Message" id="Message" rows="4" class="form-control" placeholder="Enter your message"></textarea>
                        </div>
                    </div>


                    <div class="row">
                        <div class="small-12 medium-6 large-6 columns">
                            <label for="Answer"><?php echo $singleQuestion['question']; ?></label>
                            <input type="text" name="Answer" id="Answer" class="form-control" placeholder="Enter the answer" />
                        </div>
                    </div>

                <button name="Submit" id="formSubmit" class="button">Send Message</button>

                <div id="form-response"></div>
            </form>

        <?php endwhile; endif; ?>
    </div>
</section>

<?php get_footer(); ?>
