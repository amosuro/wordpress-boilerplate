<?php

/* Table of Contents
================================================== */
// Plugin Activation
require_once locate_template('/lib/plugin-activation.php');
require_once locate_template('/lib/active-plugins.php');
// Custom Post-types & Taxonomies
require_once locate_template('/lib/cpt-taxonomy.php');
// Misc functions
require_once locate_template('/lib/misc.php');
// Enqueue Scripts
require_once locate_template('/lib/scripts.php');
// Enqueue Scripts
require_once locate_template('/lib/stylesheets.php');
// Widgets
require_once locate_template('/lib/widgets.php');
// Breadcrumbs
require_once locate_template('/lib/breadcrumbs.php');
