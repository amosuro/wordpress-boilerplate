<?php

/* Template Name: DEMO Video Gallery */

get_header(); ?>

<script type="text/javascript">
    $(function() {
        $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',

            arrows : false,
            helpers : {
                media : {},
                buttons : {}
            }
        });
    });
</script>

<section class="row">
    <div class="large-12 columns">
        <ul class="small-block-grid-2 large-block-grid-4">
            <li>
                <a class="fancybox-media" href="http://www.youtube.com/watch?v=opj24KnzrWo"><img src="http://placehold.it/500x500.jpg" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-media" href="http://vimeo.com/25634903"><img src="http://placehold.it/500x500.jpg" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-media" href="http://instagr.am/p/IejkuUGxQn"><img src="http://placehold.it/500x500.jpg" alt="" class="img-responsive" /></a>
            </li>
            <li>
                <a class="fancybox-media" href="http://www.youtube.com/watch?v=opj24KnzrWo"><img src="http://placehold.it/500x500.jpg" alt="" class="img-responsive" /></a>
            </li>
        </ul>
    </div>
</section>




<?php get_footer(); ?>
