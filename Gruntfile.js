module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
          css: {
              files: ['wp-content/themes/mosuro-foundation/_/scss/*.scss', 'wp-content/themes/mosuro-foundation/_/scss/foundation/*.scss', 'wp-content/themes/mosuro-foundation/_/scss/foundation/components/*.scss'],
              tasks: ['sass'],
              options: {
                  spawn: false,
                  sourceMap: false
              }
          }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: false
                },
                files: {
                    'wp-content/themes/mosuro-foundation/_/css/app.css':'wp-content/themes/mosuro-foundation/_/scss/app.scss'
                }
            },

            build: {
              options: {
                sourceMap: false
              }
            }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['sass']);

};
